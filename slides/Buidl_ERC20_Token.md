---
title: BUIDL an ERC20 token!
tags: Blockchain, PDX, Talk
description: Slides for the meetup Blockchain PDX, buidling an ERC20 token on Ethereum! View the slides with "Slide Mode".
---

## Welcome!

###### Snacks & drinks are available, please help yourself!

![](https://i.imgur.com/S3st6cn.png)

Join our slack group!

<small>

* Wifi: "Blockchain Meetup", Password: "<span>learnblockchain<!-- .element: class="fragment highlight-red" data-fragment-index="2" --></span>"<span style="font-size: 40px">←←←<!-- .element: class="fragment" data-fragment-index="2" --></span>
* Code of Conduct: https://gitlab.com/TrevorJTClarke/blockchain-pdx/blob/master/Code_Of_Conduct.md

</small>

<small>Thank you to our sponsors: [Amberdata.io](https://amberdata.io), [Consensys](https://www.buidlnetwork.net/), [10net](10net.net)</small>

---

## Tonight's Happenings

* Snacks & networking (How was it?)
* Show & Tell (Amberdata, Gather)
* BUIDL
* Questions & Answers

---

## Show & Tell

---

### Shameless plug 🔌

##### Amberdata.io

<small>

1. A blockchain & market data API
2. Dashboards for blockchain
3. Fun things like a Slackbot
4. Try it free & show me what you build!

</small>


---


# ERC20 Token

<!-- Put the link to this slide here so people can follow -->
Follow along →  https://hackmd.io/@1XqK8hlWRW6FMt8Q5EAXJw/erc20#/

<!-- .slide: data-transition="fade-out" -->

---

<!-- .slide: data-transition="fade-in convex-out" -->

## Tools

- Remix IDE
- Brain 🧠

----

<!-- .slide: data-transition="fade-in convex-out" -->

## Goals
- Introduce (*briefly*) the concept of ERC20 tokens & cryptoeconomics
- Build & deploy ERC20 token

----

<!-- .slide: data-transition="fade-in convex-out" -->

## Modus Operandi

- Ask lot's of questions, that's why we're here!
- Jot them down and ask at alloted interval
- Send them in slack
- lmgtfy.com/?q=your+question

---


<!-- .slide: data-background="https://steemitimages.com/0x0/https://res.cloudinary.com/hpiynhbhq/image/upload/v1521625944/tuecbnfjrmpnzmxrglnk.gif" -->

## FIRE HOSE

---

<!-- .slide: data-transition="convex-out" -->

## Cryptoeconomics

> Cryptoeconomic approaches combine cryptography and economics to create robust decentralized P2P networks that thrive over time despite adversaries attempting to disrupt the network.
> -- Nick Tomaino 

----

<!-- .slide: data-transition="fade-in convex-out" -->

### Example: Bitcoin

- transacting value (economy)
- secured via hashing (cryptography)
- distributed nodes (peer-to-peer / decentralization)

----

<!-- .slide: data-transition="fade-in convex-out" -->

## Token economics

> describes the study, design, and implementation of economic systems built on blockchain technology

----

<!-- .slide: data-transition="fade-in convex-out" -->

### Example

###### Chainlink (LINK) → wb3.io/LINK

- utility token that provides access to the chainlink ecosystem

----

<!-- .slide: data-transition="fade-in" -->


![](https://i.imgur.com/arS3ynq.png)


----

![](https://i.imgur.com/7OBlKat.png)

----

### Common Goal

###### To create incentives for stakeholders to participate

----

<!-- .slide: data-transition="fade-in convex-out" -->

## Questions? 

<!-- .slide: data-background="https://media.giphy.com/media/XHVmD4RyXgSjd8aUMb/giphy.gif" -->

---

## What's an ERC20 Token?

----

<!-- .slide: data-transition="fade-in convex-out" -->

## Inception

Fabian Vogelsteller aka frozeman

![](https://i.imgur.com/2tnzZjg.png)

----

<!-- .slide: data-transition="fade-in convex-out" -->

## Adoption

In September of 2017, EIP-20, formally adopted for Ethereum and defines a **standard interface*** for tokens. 

<small>*this will come up again </small>

----

<!-- .slide: data-transition="fade-in convex-out" -->

### Fun fact 🤓

Original method name proposals:

createCheque, cashCheque, transferCheque
tearCheque for unapprove

---

<!-- .slide: data-transition="fade-in convex-out" -->

<h2>EIP</h2>
&nbsp;
<small>(Ethereum Improvement Proposals)</small>


"...a design document providing information to the Ethereum community, or describing a new feature for Ethereum or its processes or environment."
<small>Source: [EIP 1](https://github.com/ethereum/EIPs/blob/master/EIPS/eip-1.md#what-is-an-eip)</small>


<small>

Similar to..

PEP (Python Enhancement Proposal)

BIP (Bitcoin Improvemnt Proposal)

</small>

----

<!-- .slide: data-transition="fade-in convex-out" -->

## ERC or EIP ??



<span>![](https://i.imgur.com/2gHrrnO.png) <!-- .element: class="fragment fade-out" data-fragment-index="2" --></span>

<p>EIP - Ethereum Core/Network Improvements <!-- .element: class="fragment shrink" data-fragment-index="2" --></p>

<p>ERC - Ethereum Contract Standards <!-- .element: class="fragment grow" data-fragment-index="2" --></p>

----

<!-- .slide: data-transition="fade-in convex-out" -->

#### Note: 
ERC (*Ethereum Request for Comment*)
<small>Just like RFC for web standards</small>

---

<!-- .slide: data-transition="fade-in convex-out" -->

## So an ERC-20 is...

#### The standard token interface

----

<!-- .slide: data-transition="fade-in convex-out" -->

#### Defintion

<p style="font-size: 10px">
eips.ethereum.org/EIPS/eip-20
</p>

![](https://i.imgur.com/gIA5dSw.png)


---

<!-- .slide: data-transition="fade-in convex-out" -->

## Questions? 

<!-- .slide: data-background="https://media.giphy.com/media/XHVmD4RyXgSjd8aUMb/giphy.gif" -->

---

<!-- .slide: data-transition="fade-in convex-out" -->


## Time to buidl!

----

<!-- .slide: data-transition="fade-in convex-out" -->

### Steps

<span>Contract Setup <!-- .element: class="fragment" data-fragment-index="1" --></span>

<span>Transfer <!-- .element: class="fragment" data-fragment-index="2" --></span>

<span>Approval <!-- .element: class="fragment" data-fragment-index="3" --></span>

<span>Transfer From<!-- .element: class="fragment" data-fragment-index="4" --></span>

<span>Events (Logs) <!-- .element: class="fragment" data-fragment-index="5" --></span>

<span>Interfaces <!-- .element: class="fragment" data-fragment-index="6" --></span>

----

<!-- .slide: data-transition="fade-in convex-out" -->

# [EIP20](https://eips.ethereum.org/EIPS/eip-20)

---

### Contract Setup


Tasks:

- Declare the pragma & contract
- Declare state variables
- Implement constructor

----

<!-- .slide: data-transition="fade-in convex-out" -->

## To [Remix](https://remix.ethereum.org/)!

---

<!-- .slide: data-transition="fade-in convex-out" -->

## Questions? 

<!-- .slide: data-background="https://media.giphy.com/media/XHVmD4RyXgSjd8aUMb/giphy.gif" -->

---

### Transfer

###### Setup the ablility to transfer tokens between addresses

Task:
- Implement `transfer` & `balanceOf` methods


----

###### Function Modifiers

> Modifiers can be used to easily change the behaviour of functions. Modifiers are inheritable properties of contracts and may be overridden by derived contracts.
> &mdash; Solidity Docs


----

### the code

```
function transfer(address _to, uint256 _value) public returns (bool success) {
        // prevent overspending
        
        // update balances

        return
}
```

----

## Back to Remix...

---

<!-- .slide: data-transition="fade-in convex-out" -->

## Questions? 

<!-- .slide: data-background="https://media.giphy.com/media/XHVmD4RyXgSjd8aUMb/giphy.gif" -->

---

### Approve

###### Allow for other addresses to spend tokens on the behalf of another address.

Task:
- Implement `approve` & `allowance` methods

----

## Back to Remix...

---

## Questions? 

<!-- .slide: data-background="https://media.giphy.com/media/XHVmD4RyXgSjd8aUMb/giphy.gif" -->

---

### Transfer From

Task:
- Implement `transferFrom` method

----

### the code

```
function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
 
        // get allowance
        
        // require that value be < balances and allowance
        
        // update balances
        
        // check that allowance < MAX_UINT256
        
            // deduct from allowance
            
        return
    }
```

----

## Back to Remix...

---

## Questions? 

<!-- .slide: data-background="https://media.giphy.com/media/XHVmD4RyXgSjd8aUMb/giphy.gif" -->

---

### Events

Task:
- Define and emit `Approval` & `Transfer` events

----

## Back to Remix...

---

## Questions? 

<!-- .slide: data-background="https://media.giphy.com/media/XHVmD4RyXgSjd8aUMb/giphy.gif" -->

---

### Interface

Task:
- Define the interface

----

## Back to Remix...

---

## Questions? 

<!-- .slide: data-background="https://media.giphy.com/media/XHVmD4RyXgSjd8aUMb/giphy.gif" -->

---

## Conculsion

---

# Resources

* [Meetup Archive Repository](https://gitlab.com/TrevorJTClarke/blockchain-pdx)
* [Gitlab Slides](https://hackmd.io/@trevor/buidldapp-part2#/)
* [Circles - dApp](https://github.com/TrevorJTClarke/Circles)
* [Remix IDE](https://remix.ethereum.org)
* [Amberdata API](https://docs.amberdata.io/)
